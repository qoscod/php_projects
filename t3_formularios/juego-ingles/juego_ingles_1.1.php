
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    body{
        margin: 50px 50px;
    }
    
    p{
        //display:inline-block;
    }
    input[type="text"]{
        position:absolute;
        left:150px;
    }
    input[type="submit"]{
        position:relative;
        left:300px;
        bottom:35px;
    }
    form{
        background-color: light-grey;
    }
</style>
<body>
<?php
    $arraySoluciones = array(0 => array("begin","began","begun"),1 => array("go","went","gone"),2 => array("become","became","become"));
    $arrayVerbos = array("Comenzar","Ir","Convertirse");

    function contar_aciertos(){
        global $indice;
        global $arraySoluciones;
        $aciertos=0;
        foreach ($_POST as $key => $respuesta){
            if ($respuesta === $arraySoluciones[$indice][$key]){
                $aciertos += 1;
            }
        }
        return $aciertos;
    }

    function print_form(){
        global $indice;
        global $arrayVerbos;
        global $aciertos;
        global $intentos;
    
        echo <<<EOD
        <p>Verbo $indice:<b>$arrayVerbos[$indice]</b></p>
        <form action="?indice=$indice&aciertos=$aciertos&intentos=$intentos" method="post" enctype="application/x-www-form-urlencoded"> 
            <label for="0">Infinitivo</label>
            <input type="text" id="0" name="0" /> <br>
            <label for="1">Pasado simple </label>
            <input type="text" id="1" name="1" /> <br>
            <label for="2">Participio </label>
            <input type="text" id="2" name="2" /> <br>
            <input type="submit" />
        </form>
EOD;
    }

    if(!isset($_GET['indice']) && !isset($_GET['aciertos'])){
        $indice=0;
        $aciertos=0;
        $errores=0;
        $intentos=0;

    }else{
        $indice = $_GET['indice'];
        $intentos = $_GET['intentos'];
        $aciertos = $_GET['aciertos'];
        $aciertos += contar_aciertos();
        $errores = ($indice+1)*3-$aciertos;
        $indice +=1;
        
        if($indice == count($arrayVerbos)){
            $indice=0; 
            $intentos ++;
        } 
    }


        print_form();
        print "<p>Aciertos:".$aciertos."  Errores:".$errores."  Intentos:".$intentos."<p>";
?>
</body>
</html>



