<?php 
$imagen = imagecreate(500,500);
$rojo = imagecolorallocate($imagen,150,0,0);
$azul = imagecolorallocate($imagen,0,0,100);
$blanco = imagecolorallocate($imagen,0,0,0);
imagefilledrectangle($imagen, 450, 450, 50, 50, $azul);
imagefilledrectangle($imagen, 400, 400, 100, 100, $rojo);
imagefilledrectangle($imagen, 350, 350, 150, 150, $azul);
imagefilledrectangle($imagen, 300, 300, 200, 200, $rojo);

header("Content-type:image/jpeg");
imagejpeg($imagen);
imagedestroy($imagen);
?>
