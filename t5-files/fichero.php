<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<form action="#" method="post" enctype="multipart/form-data">
    <?php
    echo "Nombre usuario:<input type='text' name='usuario'/><br/>";
    echo "Fichero con su fotografía:<input type='file' name='imagen' accept='.gif,.jpeg,.png'/><br/>";
    ?>
    <input type="submit" value="Enviar">

</form>
<?php

if (!empty($_FILES['imagen'])){
    echo "name:".$_FILES['imagen']['name']."<br>";
    echo "tmp_name:".$_FILES['imagen']['tmp_name']."<br>";
    echo "size:".$_FILES['imagen']['size']."<br>";
    echo "type:".$_FILES['imagen']['type']."<br>";
    if (is_uploaded_file ($_FILES['imagen']['tmp_name'] )){
        $nombreDirectorio = "img/";
        $nombreFichero = $_FILES['imagen']['name'];
        $nombreCompleto = $nombreDirectorio.$nombreFichero;
    if (is_dir($nombreDirectorio)){ // es un directorio existente
        $idUnico = time();
        $nombreFichero = $idUnico."-".$nombreFichero;
        $nombreCompleto = $nombreDirectorio.$nombreFichero;
        move_uploaded_file ($_FILES['imagen']['tmp_name'],$nombreCompleto);
        echo "Fichero subido con el nombre: $nombreFichero<br>";
    }
    else echo 'Directorio definitivo inválido';
    }
    else
    print ("No se ha podido subir el fichero\n");
}

?>
</body>
</html>