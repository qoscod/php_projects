
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/forms.css">
    <title>Document</title>
</head>
<body>
    <?php
    function seleccionaCartas ($num,$baraja){
        $randIndex = array_rand($baraja,$num);
        foreach ($randIndex as $key => $value) {
            $res[] = $baraja[$value];
        }
        return $res;
    }
    function crearBarajaEspañola(&$matriz){
        $palos=array(0 => "oros",10 => "bastos",20 => "espadas",30 => "copas",);
        $figuras=array(1 => "As",8 => "Sota",9 => "Caballo",10 => "Rey",);
        foreach ($matriz as $key => $value) {
            switch ($value) {
                case (in_array($value, range(1,10))):
                    $palo =$palos[0];
                    $nPalo=0;
                    break;
                case (in_array($value, range(11,20))):
                    $palo =$palos[10];
                    $nPalo =10;
                    break;
                case (in_array($value, range(21,30))):
                    $palo =$palos[20];
                    $nPalo=20;
                    break;
                case (in_array($value, range(31,40))):
                    $palo =$palos[30];
                    $nPalo =30;
                    break;
                default:
                    # code...
                    break;
            }
        
            $carta=$value-$nPalo;
            switch ($carta) {
                case 1:
                    $matriz[$key]="$figuras[1] de $palo";
                    break;
                case 8:
                    $matriz[$key]="$figuras[8] de $palo";
                    break;
                case 9:
                    $matriz[$key]="$figuras[9] de $palo";
                    break;
                case 10:
                    $matriz[$key]="$figuras[10] de $palo";
                    break;
                default:
                    $matriz[$key]="$carta de $palo";
                    break;
            }
        }
    }
    
    require 'matrix_functions.php';
    function printTablero($matriz){
        echo "<table>";
        foreach($matriz as $fila => $casillas){
            echo"<tr>";
            foreach($casillas as $columna =>$casilla){
                    switch ($fila){
                            case 2:
                            case 7:
                            $figura="peon";
                            break;
                        case 1:
                        case 8:
                            if($columna == 1 || $columna == 8 )$figura="torre";
                            else if($columna == 2 || $columna == 7 )$figura="alfil";
                            else if($columna == 3 || $columna == 6 )$figura="caballo";
                            else if($columna == 4)$figura="rey";
                            else if($columna == 5)$figura="reina";
                            break;
                        default:
                            $figura = "";
                        }
                if (($fila+$casilla)%2 == 0){
                    echo "<td class=\"black\">$figura</td>";
                }else echo "<td class=\"white\">$figura</td>";
            }
            echo "</tr>";
        }
        echo "</table>";
    }
    if(isset($_POST['baraja'])){
        $baraja=unserialize($_POST['baraja']);
        if (isset($_POST['barajar'])) {
            shuffle($baraja);
        }
    }else{
        $baraja = array_merge(...randMatrix(12,48,false));
    }
    
    ?>
    <div class="content">
        <form action="#" method="post">
            <div class="buttonAndText">
            <input type="submit" name="ver" value="Ver ">
            <input type="number" name="number" id="number" placeholder="Número de cartas">
            </div>
            <input type="submit" name="verBaraja"value="Ver baraja">
            <input type="submit" name="barajar" value="Barajar mazo">
            <input type="submit" name="tablero" value="Generar tablero">
            <input type="hidden" name="baraja" value="<?php echo serialize($baraja)?>">
        </form>
        <?php 
       
        if (isset($_POST['ver']) && !empty($_POST['number'])){
            foreach (seleccionaCartas($_POST['number'],$baraja) as $key => $carta){
                echo ("<img src=\"./img/".$carta."\" width=100px>");
            }
        }
        if (isset($_POST['verBaraja'])) {
                foreach ($baraja as $key => $carta) {
                    echo ("<img src=\""."./img/"."$carta.jpg \""." width=\"100px\">");
                }
        }
        if (isset($_POST['tablero'])){
            $tablero = randMatrix(8,64,false);
            printTablero($tablero);
        }
        if (isset($_POST['barajar'])){
            echo "Mazo barajeado";
        }
        ?>  
    </div>   
</body>
</html>

