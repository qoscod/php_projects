<?php

function randMatrix($valuesPerLine,$maxValue,$random){
    $randValues = array();
    $randNumb = 0;
    for ($i=0; $i < $maxValue; $i++) { 
        $break = false;
        while($break == false){
            if($random == false){
                $randNumb++;
                $randValues[] =$randNumb;
                $break=true;
            }
            else {
                $randNumb = rand(1,$maxValue);
                if(!in_array($randNumb,$randValues,false)){
                        $randValues[] =$randNumb;
                        $break=true;
                }
            }
        }
    }
    $matrix =array();
    $line=1;
    $column =0;
    foreach($randValues as $value){
         $matrix[$line][++$column]=$value;
        if ($column == $valuesPerLine){
            $column=0;
            $line++;
        }
    }
 return $matrix;
}


?>